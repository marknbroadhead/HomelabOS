# Gitea

[Gitea](https://gitea.io/en-US/) is a Git hosting platform.

## Access

It is available at [http://git.{{ domain }}/](http://git.{{ domain }}/)