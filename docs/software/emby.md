# Emby

[Emby](https://emby.media/) is a media server. Just point it at your NAS collections of Movies and TV and you're off to the races.

## Access

It is available at [http://emby.{{ domain }}/](http://emby.{{ domain }}/)