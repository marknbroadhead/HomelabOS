# Grafana

[Grafana](https://grafana.com/) is a Time Series Database graphing application.

You can use it to visualize the Weather data imported by [influxdb_darksky](software/influxdb_darksky)

## Access

It is available at [http://grafana.{{ domain }}/](http://grafana.{{ domain }}/)